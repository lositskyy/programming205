﻿using System;
using System.Threading;

namespace ConsoleApp1
{
    class Program
    {
        //Створити консольний застосунок, який запускає паралельні обчислення у потоках на обробку масивів різного типу та довжини. 
        //Тип задачі на обробку масиву підібрати самостійно (або з викладачем)
        //Затримку внутрі функцій емулювати за допомогою Thread.Sleep
        public class Geek
        {
            public static void method1()
            {
                for (int i = 0; i <= 10; i++)
                {
                    Console.WriteLine("method1 is: {0}", i);
                    if(i == 5)
                    {
                        Thread.Sleep(10000);
                    }
                }  
            }

            public static void method2()
            {
                for (int j = 0; j <= 10; j++)
                {
                    Console.WriteLine("Method2 is {0}", j);
                }
            }
            static void Main(string[] args)
            {
                int[] nams1 = new int[] { 10, 21, 50};
                int[] nams2 = new int[5];
                //Thread thr1 = new Thread(method1);
                // Thread thr2 = new Thread(method2);
                //thr1.Start();
                // thr2.Start();
            }
        }
    }
}
